import com.google.protobuf.gradle.generateProtoTasks
import com.google.protobuf.gradle.id
import com.google.protobuf.gradle.ofSourceSet
import com.google.protobuf.gradle.plugins
import com.google.protobuf.gradle.protobuf
import com.google.protobuf.gradle.protoc

plugins {
    kotlin("jvm") version "1.5.10"
    java
    id("com.google.protobuf") version "0.8.16"
}

group = "org.example"
version = "1.0-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

val grpcVersion = "1.38.0"

//repositories {
//    mavenCentral()
//}

repositories {
    val artifactory_repository: String by project
    val artifactory_user: String by project
    val artifactory_password: String by project

    mavenLocal()
    maven {
        url = uri(artifactory_repository)
        credentials {
            username = artifactory_user
            password = artifactory_password
        }
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("io.grpc:grpc-protobuf:${grpcVersion}")
    implementation("io.grpc:grpc-stub:${grpcVersion}")
    implementation("io.grpc:grpc-netty-shaded:${grpcVersion}")
    implementation("javax.annotation:javax.annotation-api:1.3.2")

    testImplementation("io.mockk:mockk:1.9.3")
    testImplementation("org.assertj:assertj-core:3.11.1")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.4.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.4.2")

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.4.2")
    testRuntimeOnly("org.junit.platform:junit-platform-console:1.7.2")
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:3.17.3"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:${grpcVersion}"
        }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                id("grpc")
            }
        }
    }
}

sourceSets {
    main {
        java {
            srcDir(
                listOf(
                    "build/generated/source/proto/main/java",
                    "build/generated/source/proto/main/grpc"
                )
            )
        }
    }
}

tasks.jar {
    manifest {
        attributes(
            mapOf(
                "Main-Class" to "org.example.MainKt"
            )
        )
    }

    configurations["compileClasspath"].forEach { file: File ->
        from(zipTree(file.absoluteFile))
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}