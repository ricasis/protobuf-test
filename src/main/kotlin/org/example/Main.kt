package org.example

import org.example.application.config.server.GRPCServer

private val grpcServer: GRPCServer = GRPCServer()

fun main() {
    try {
        grpcServer.startServer()
        println("Server started")
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        grpcServer.stopServer()
        println("Server stopped")
    }
}