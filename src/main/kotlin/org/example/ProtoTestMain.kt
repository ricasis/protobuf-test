package org.example

import org.example.resource.Credential
import org.example.resource.common.Address
import org.example.resource.common.Car
import org.example.resource.Dealer
import org.example.resource.EmailCredential
import org.example.resource.Person
import org.example.resource.PhoneOTP
import org.example.resource.common.CarType

fun main() {
    val dealer = Dealer.newBuilder()
        .putModel(1, Car.newBuilder()
            .setBrand("Test Brand")
            .setModel("Model A")
            .setYear(2021)
            .setType(CarType.SEDAN)
            .build()
        ).build()

    println(dealer)

    val person = Person.newBuilder()
        .setName("Ricardo")
        .setAge(27)
        .setAddress(
            Address.newBuilder()
                .setCep(4849160)
                .setCity("São Paulo")
                .setStreet("Manuel Plá")
                .build()
        ).addAllCar(
            listOf(
                Car.newBuilder()
                    .setBrand("Test Brand")
                    .setModel("Model A")
                    .setYear(2021)
                    .build()
            )
        ).build()

    println("Person:\n$person")

    doLogin()
}

fun doLogin() {
    val emailCred = EmailCredential.newBuilder()
        .setEmail("teste@test.com")
        .setPassword("123")
        .build()

    val phoneOTP = PhoneOTP.newBuilder()
        .setNumber("1234")
        .setCode("12")
        .build()

    val credential = Credential.newBuilder()
        .setEmailMode(emailCred)
        .setPhoneMode(phoneOTP)
        .build()

    login(credential)
}

fun login(credential: Credential) {
    if (credential.hasEmailMode()) println("Email ${credential.phoneMode}")
    if (credential.hasPhoneMode()) println("Phone ${credential.emailMode}")
    println(credential)
}