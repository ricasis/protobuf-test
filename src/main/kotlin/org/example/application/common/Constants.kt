package org.example.application.common

import io.grpc.Context
import io.grpc.Metadata
import io.grpc.protobuf.ProtoUtils
import org.example.resource.service.WithdrawalError

object Constants {

    val TOKEN_METADATA = Metadata().apply {
        put(Metadata.Key.of("auth-token", Metadata.ASCII_STRING_MARSHALLER), "client-token")
    }
    val WITHDRAWAL_ERROR_KEY = ProtoUtils.keyForProto(WithdrawalError.getDefaultInstance())

    val CTX_USER_ROLE: Context.Key<String> = Context.key("user-role")
}