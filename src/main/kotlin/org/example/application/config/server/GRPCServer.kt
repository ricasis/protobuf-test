package org.example.application.config.server

import io.grpc.ServerBuilder
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder
import io.grpc.netty.shaded.io.netty.handler.ssl.SslContextBuilder
import org.example.application.interceptor.AuthInterceptor
import org.example.domain.service.BankService
import org.example.domain.service.TransferService
import java.io.File

class GRPCServer {

    private val sslContext = GrpcSslContexts.configure(
        SslContextBuilder.forServer(
            File("./certs/localhost.crt"),
            File("./certs/localhost.pem")
        )
    ).build()

    private val server = NettyServerBuilder.forPort(7000)
        .sslContext(sslContext)
        .intercept(AuthInterceptor())
        .addService(BankService())
        .addService(TransferService())
        .build()

    fun startServer() {
        server.run {
            start()
//            awaitTermination()
        }
    }

    fun stopServer() {
        server.run {
            shutdown()
        }
    }
}