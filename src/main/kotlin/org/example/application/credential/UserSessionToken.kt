package org.example.application.credential

import io.grpc.CallCredentials
import io.grpc.Metadata
import java.util.concurrent.Executor

class UserSessionToken(
    private val jwt: String
): CallCredentials() {

    override fun applyRequestMetadata(requestInfo: RequestInfo?, appExecutor: Executor?, applier: MetadataApplier?) {
        appExecutor?.execute {
            val metadata = Metadata().apply {
                put(USER_TOKEN, jwt)
            }
            applier?.apply(metadata)
            //applier.fail()
        }
    }

    override fun thisUsesUnstableApi() {
        TODO("Not yet implemented")
    }

    companion object {
        private val USER_TOKEN = Metadata.Key.of("user-token", Metadata.ASCII_STRING_MARSHALLER)
    }
}