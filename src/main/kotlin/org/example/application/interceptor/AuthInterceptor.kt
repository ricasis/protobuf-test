package org.example.application.interceptor

import io.grpc.Context
import io.grpc.Contexts
import io.grpc.Metadata
import io.grpc.ServerCall
import io.grpc.ServerCallHandler
import io.grpc.ServerInterceptor
import io.grpc.Status
import org.example.application.common.Constants.CTX_USER_ROLE
import java.util.concurrent.ThreadLocalRandom

class AuthInterceptor: ServerInterceptor {
    override fun <ReqT : Any?, RespT : Any?> interceptCall(
        call: ServerCall<ReqT, RespT>?,
        headers: Metadata?,
        next: ServerCallHandler<ReqT, RespT>?
    ): ServerCall.Listener<ReqT> {
        val invalidStatus = Status.UNAUTHENTICATED.withDescription("Invalid token")

        if (next == null || call == null || headers == null)
            throw IllegalArgumentException("ServerCallHandler cannot be null")

        val token = headers.get(TOKEN_METADATA_KEY) ?:
                    headers.get(USER_TOKEN_METADATA)

        return token?.let {
            if (validateToken(it)) {
                val context = ThreadLocalRandom.current().nextInt(1, 2).let { role ->
                    Context.current().withValue(CTX_USER_ROLE, if (role == 1) "PRIME" else "STANDARD")
                }
                Contexts.interceptCall(context, call, headers, next)
//                next.startCall(call, headers)
            } else {
                call.close(invalidStatus, headers)
                object: ServerCall.Listener<ReqT>(){}
            }
        } ?: run {
            call.close(invalidStatus, headers)
            object: ServerCall.Listener<ReqT>(){}
        }
    }

    private fun validateToken(token: String) = VALID_TOKEN == token || USER_VALID_TOKEN == token

    companion object {
        private const val VALID_TOKEN = "client-token"
        private const val USER_VALID_TOKEN = "user-token"
        private val TOKEN_METADATA_KEY = Metadata.Key.of("auth-token", Metadata.ASCII_STRING_MARSHALLER)
        private val USER_TOKEN_METADATA = Metadata.Key.of("user-token", Metadata.ASCII_STRING_MARSHALLER)
    }
}