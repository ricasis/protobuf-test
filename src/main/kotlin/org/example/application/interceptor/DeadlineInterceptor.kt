package org.example.application.interceptor

import io.grpc.CallOptions
import io.grpc.Channel
import io.grpc.ClientCall
import io.grpc.ClientInterceptor
import io.grpc.Deadline
import io.grpc.MethodDescriptor
import java.util.concurrent.TimeUnit

class DeadlineInterceptor: ClientInterceptor {
    override fun <ReqT : Any?, RespT : Any?> interceptCall(
        method: MethodDescriptor<ReqT, RespT>?,
        callOptions: CallOptions?,
        next: Channel?
    ): ClientCall<ReqT, RespT> {
        val deadline = callOptions?.deadline ?: Deadline.after(30, TimeUnit.SECONDS)
        return next?.newCall(
            method, callOptions?.withDeadline(deadline)
        ) ?: throw IllegalArgumentException("Channel is mandatory to execute RPC")
    }
}