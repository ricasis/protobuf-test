package org.example.domain.service

import com.google.common.util.concurrent.Uninterruptibles
import io.grpc.Context
import io.grpc.Metadata
import io.grpc.Status
import io.grpc.protobuf.ProtoUtils
import io.grpc.stub.StreamObserver
import org.example.application.common.Constants.CTX_USER_ROLE
import org.example.resource.DepositStreamObserverRequest
import org.example.resource.service.Balance
import org.example.resource.service.BalanceCheckRequest
import org.example.resource.service.BankServiceGrpc
import org.example.resource.service.DepositRequest
import org.example.resource.service.ErrorMessage
import org.example.resource.service.Money
import org.example.resource.service.WithdrawRequest
import org.example.resource.service.WithdrawalError
import java.lang.IllegalArgumentException
import java.util.concurrent.TimeUnit

class BankService : BankServiceGrpc.BankServiceImplBase() {

    override fun getBalance(request: BalanceCheckRequest?, responseObserver: StreamObserver<Balance>?) {
        try {
            Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS)
            request?.accountNumber?.let {
                val balance = Balance.newBuilder().setAmount(it * 10.0).build()
                responseObserver?.run {
                    onNext(balance)
                    onCompleted()
                }
            } ?: throw IllegalArgumentException("Account number is mandatory")
        } catch (e: Exception) {
            e.printStackTrace()
            responseObserver?.onError(e)
        }
    }

    override fun withdraw(request: WithdrawRequest?, responseObserver: StreamObserver<Money>?) {
        try {
            request?.accountNumber?.let {
                println("User role: ${CTX_USER_ROLE.get()}")
                if (it == 10) {
                    val errorKey = ProtoUtils.keyForProto(WithdrawalError.getDefaultInstance())
                    val error = WithdrawalError.newBuilder()
                        .setAmount(request.amount)
                        .setErrorMessage(ErrorMessage.INSUFFICIENT_BALANCE)
                        .build()
                    val metadataError = Metadata().apply {
                        put(errorKey, error)
                    }
//                    val status = Status.FAILED_PRECONDITION.withDescription("Account $it does not have balance")
//                    responseObserver?.onError(status.asRuntimeException())
                    responseObserver?.onError(Status.FAILED_PRECONDITION.asRuntimeException(metadataError))
                    return
                } else {
                    var amount = request.amount

                    if ((amount % 10.0) != 0.0) {
                        val errorKey = ProtoUtils.keyForProto(WithdrawalError.getDefaultInstance())
                        val error = WithdrawalError.newBuilder()
                            .setAmount(request.amount)
                            .setErrorMessage(ErrorMessage.ONLY_TEN_MULTIPLES)
                            .build()
                        val metadataError = Metadata().apply {
                            put(errorKey, error)
                        }
                        responseObserver?.onError(Status.FAILED_PRECONDITION.asRuntimeException(metadataError))
                        return
                    }

                    while (amount > 0) {
//                        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS)
                        if (Context.current().isCancelled) break
                        amount -= 10
                        val money = Money.newBuilder().setValue(10.0).build()
                        responseObserver?.onNext(money)
                        Thread.sleep(1000)
                    }
                    responseObserver?.onCompleted()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            responseObserver?.onError(e)
        }
    }

    override fun deposit(responseObserver: StreamObserver<Balance>?): StreamObserver<DepositRequest> {
        return responseObserver?.let {
            DepositStreamObserverRequest(it)
        } ?: throw Exception("Observer parameter is null")
    }
}