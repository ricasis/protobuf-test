package org.example.domain.service

import io.grpc.stub.StreamObserver
import org.example.resource.TransferStreamObserverRequest
import org.example.resource.service.TransferRequest
import org.example.resource.service.TransferResponse
import org.example.resource.service.TransferServiceGrpc
import java.lang.Exception

class TransferService: TransferServiceGrpc.TransferServiceImplBase() {

    override fun transfer(responseObserver: StreamObserver<TransferResponse>?): StreamObserver<TransferRequest> {
        return responseObserver?.let {
            TransferStreamObserverRequest(it)
        } ?: throw Exception("No responseObserver")
    }
}