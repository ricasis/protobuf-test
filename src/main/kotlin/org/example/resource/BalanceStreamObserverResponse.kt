package org.example.resource

import io.grpc.stub.StreamObserver
import org.example.resource.service.Balance
import java.util.concurrent.CountDownLatch

class BalanceStreamObserverResponse(
    private val countDownLatch: CountDownLatch
): StreamObserver<Balance> {
    override fun onNext(value: Balance?) {
        println("Balance received: ${value?.amount}")
    }

    override fun onError(t: Throwable?) {
        t?.printStackTrace()
        countDownLatch.countDown()
    }

    override fun onCompleted() {
        println("Response received!")
        countDownLatch.countDown()
    }
}