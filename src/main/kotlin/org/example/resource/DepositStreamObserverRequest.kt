package org.example.resource

import io.grpc.stub.StreamObserver
import org.example.resource.service.Balance
import org.example.resource.service.DepositRequest

class DepositStreamObserverRequest(
    private val balanceStreamObserver: StreamObserver<Balance>
): StreamObserver<DepositRequest> {

    private var balance: Double = 0.0

    override fun onNext(depositRequest: DepositRequest?) {
        depositRequest?.accountNumber?.let {
            println("Depositing ${depositRequest.amount} to account $it")
            balance += depositRequest.amount
        }
    }

    override fun onError(t: Throwable?) {
        t?.printStackTrace()
    }

    override fun onCompleted() {
        val balanceResponse = Balance.newBuilder().setAmount(balance).build()
        balanceStreamObserver.onNext(balanceResponse)
        balanceStreamObserver.onCompleted()
    }
}