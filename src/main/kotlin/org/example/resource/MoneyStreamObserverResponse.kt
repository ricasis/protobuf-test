package org.example.resource

import io.grpc.Status
import io.grpc.stub.StreamObserver
import org.example.application.common.Constants.WITHDRAWAL_ERROR_KEY
import org.example.resource.service.Money
import java.util.concurrent.CountDownLatch

class MoneyStreamObserverResponse(
    private val countDownLatch: CountDownLatch
): StreamObserver<Money> {

    override fun onNext(value: Money?) {
        println("Received ${value?.value}")
    }

    override fun onError(t: Throwable?) {
        t?.printStackTrace()
        val status = Status.fromThrowable(t)
        val headers = Status.trailersFromThrowable(t)
        headers?.get(WITHDRAWAL_ERROR_KEY)?.let {
            println("Error: ${it.errorMessage}. Amount: ${it.amount}. Status: $status")
        }
        countDownLatch.countDown()
    }

    override fun onCompleted() {
        println("Streaming complete")
        countDownLatch.countDown()
    }
}