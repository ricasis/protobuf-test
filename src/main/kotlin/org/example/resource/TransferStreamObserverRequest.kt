package org.example.resource

import io.grpc.stub.StreamObserver
import org.example.resource.service.Account
import org.example.resource.service.TransferRequest
import org.example.resource.service.TransferResponse
import org.example.resource.service.TransferStatus

class TransferStreamObserverRequest(
    private val transferStreamResponse: StreamObserver<TransferResponse>
): StreamObserver<TransferRequest> {

    private var totalAmount = 0

    override fun onNext(request: TransferRequest?) {
        println(
            "Received request to transfer from ${request?.fromAccount} to ${request?.toAccount}. " +
                "Value: ${request?.amount}"
        )
        totalAmount += request?.amount ?: 0

        val accounts = listOf(
            Account.newBuilder().setAccountNumber(request!!.fromAccount).setAmount(10).build(),
            Account.newBuilder().setAccountNumber(request.toAccount).setAmount(20).build()
        )

        TransferResponse.newBuilder()
            .setStatus(TransferStatus.SUCCESS)
            .addAllAccounts(accounts)
            .build().let {
                transferStreamResponse.onNext(it)
            }
    }

    override fun onError(t: Throwable?) {
        t?.printStackTrace()
    }

    override fun onCompleted() {
        println("Server Total $totalAmount")
        transferStreamResponse.onCompleted()
    }
}