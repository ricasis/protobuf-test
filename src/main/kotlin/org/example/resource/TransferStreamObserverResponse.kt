package org.example.resource

import io.grpc.stub.StreamObserver
import org.example.resource.service.TransferRequest
import org.example.resource.service.TransferResponse
import java.util.concurrent.CountDownLatch

class TransferStreamObserverResponse(
    private val countDownLatch: CountDownLatch
): StreamObserver<TransferResponse> {

    override fun onNext(response: TransferResponse?) {
        println("Transfer response status: ${response?.status} and ${response?.accountsCount}")
        println("---------------------------------")
    }

    override fun onError(t: Throwable?) {
        t?.printStackTrace()
        countDownLatch.countDown()
    }

    override fun onCompleted() {
        println("Client Finished")
        countDownLatch.countDown()
    }
}