package org.example.resource

import io.grpc.Deadline
import io.grpc.ManagedChannelBuilder
import io.grpc.Status
import io.grpc.StatusRuntimeException
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder
import io.grpc.stub.MetadataUtils
import org.assertj.core.api.Assertions.assertThat
import org.example.application.interceptor.DeadlineInterceptor
import org.example.application.common.Constants
import org.example.application.config.server.GRPCServer
import org.example.application.credential.UserSessionToken
import org.example.resource.service.BalanceCheckRequest
import org.example.resource.service.BankServiceGrpc
import org.example.resource.service.DepositRequest
import org.example.resource.service.WithdrawRequest
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BankClientTest {

    private var blockingStub: BankServiceGrpc.BankServiceBlockingStub? = null
    private var asyncStub: BankServiceGrpc.BankServiceStub? = null
    private val grpcServer: GRPCServer = GRPCServer()

    @BeforeAll
    fun setup() {
        grpcServer.startServer()

        val sslContexts = GrpcSslContexts.forClient().trustManager(
            File("./certs/ca.cert.pem")
        ).build()

//        val managedChannel = ManagedChannelBuilder
//            .forAddress("localhost", 7000)
//            .intercept(MetadataUtils.newAttachHeadersInterceptor(Constants.TOKEN_METADATA))
//            .intercept(DeadlineInterceptor())
//            .usePlaintext()
//            .build()

        val managedChannel = NettyChannelBuilder
            .forAddress("localhost", 7000)
            .intercept(MetadataUtils.newAttachHeadersInterceptor(Constants.TOKEN_METADATA))
            .intercept(DeadlineInterceptor())
            .sslContext(sslContexts)
            .build()

        blockingStub = BankServiceGrpc.newBlockingStub(managedChannel)
        asyncStub = BankServiceGrpc.newStub(managedChannel)
    }

    @AfterAll
    fun teardown() {
        grpcServer.stopServer()
    }

    @Test
    fun balanceTest() {
        val request = BalanceCheckRequest.newBuilder().setAccountNumber(10).build()
        val balance = blockingStub?.withDeadline(Deadline.after(2, TimeUnit.SECONDS))?.getBalance(request)
        assertThat(balance?.amount).isEqualTo(request.accountNumber * 10.0)
    }

    @Test
    fun withdrawSyncTest() {
        val withdrawRequest = WithdrawRequest.newBuilder().setAccountNumber(8).setAmount(100.0).build()
        try {
            blockingStub?.withCallCredentials(UserSessionToken("user-token"))
                ?.withdraw(withdrawRequest)?.forEachRemaining {
                    println("Received: ${it.value}")
                    assertThat(it.value).isEqualTo(10.0)
                }
        } catch (e: Throwable) {
            val status = Status.fromThrowable(e)
            val headers = Status.trailersFromThrowable(e)
            headers?.get(Constants.WITHDRAWAL_ERROR_KEY)?.let {
                print("Error: ${it.errorMessage}. Amount: ${it.amount}. Status: $status")
            }
            throw e
        }
    }

    @Test
    fun withdrawAsyncTest() {
        val countDownLatch = CountDownLatch(1)
        val withdrawRequest = WithdrawRequest.newBuilder().setAccountNumber(8).setAmount(100.0).build()
        asyncStub?.withdraw(withdrawRequest, MoneyStreamObserverResponse(countDownLatch))
        countDownLatch.await()
    }

    @Test
    fun depositTest() {
        val countDownLatch = CountDownLatch(1)
        asyncStub?.deposit(BalanceStreamObserverResponse(countDownLatch)).let {
            for (i in 1..10) {
                val depositRequest = DepositRequest.newBuilder().setAccountNumber(10).setAmount(15.0).build()
                it?.onNext(depositRequest)
            }
            it?.onCompleted()
        }
        countDownLatch.await()
    }
}