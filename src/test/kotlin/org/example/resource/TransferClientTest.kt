package org.example.resource

import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder
import org.example.application.config.server.GRPCServer
import org.example.resource.service.TransferRequest
import org.example.resource.service.TransferServiceGrpc
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File
import java.util.concurrent.CountDownLatch

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransferClientTest {

    private var asyncStub: TransferServiceGrpc.TransferServiceStub? = null
    private val grpcServer: GRPCServer = GRPCServer()

    @BeforeAll
    fun setup() {
        grpcServer.startServer()

        val sslContexts = GrpcSslContexts.forClient().trustManager(
            File("./certs/ca.cert.pem")
        ).build()

//        val managedChannel = ManagedChannelBuilder
//            .forAddress("localhost", 7000)
//            .usePlaintext()
//            .build()

        val managedChannel = NettyChannelBuilder
            .forAddress("localhost", 7000)
            .sslContext(sslContexts)
            .build()

        asyncStub = TransferServiceGrpc.newStub(managedChannel)
    }

    @AfterAll
    fun teardown() {
        grpcServer.stopServer()
    }

    @Test
    fun transferAsyncTest() {
        val countDownLatch = CountDownLatch(1)
        var totalAmount = 0

        asyncStub?.transfer(
            TransferStreamObserverResponse(countDownLatch = countDownLatch)
        ).let { transferStreamRequest ->
            for (i in 1..10) {
                TransferRequest.newBuilder()
                    .setFromAccount(1234)
                    .setToAccount(4321)
                    .setAmount(10)
                    .build().let {
                        transferStreamRequest?.onNext(it)
                    }

                totalAmount += 10
            }
            transferStreamRequest?.onCompleted()
        }
        countDownLatch.await()
        println("Client Total $totalAmount")
    }
}